# BaySim Attachment Downloader #

Dieses Programm lädt automatisiert alle Attachment Dateien aus BaySim herunter. Alle Dateien werden, falls vorhanden, mit der entsprechenden Fallnummer im Namen herunter, sodass eine eindeutige zuordnung möglich ist.

### Benötigte Programme ###

* Laden Sie Anaconda für Ihr Betriebssystem herunter ([Link](https://www.anaconda.com/products/individual))
* Laden Sie den Firefox Gecko Treiber für Ihr Betriebssystem herunter ([Link](https://github.com/mozilla/geckodriver/releases)).
* Entpacken Sie den Firefox Gecko Treiber an einen Ort Ihrer Wahl.
* Laden Sie dieses Repository herunter (Klonen oder als Zip Datei herunterladen).

### Installation der Python Umgebung ###

1) Öffnen Sie die Anaconda Shell.
2) Navigieren Sie mit der Shell in den src Ordner des heruntergeladenen Repositoryverzeichnisses.
3) Führen Sie den nachfolgenden Befehl aus und ersetzen Sie "Umgebungsname" durch einen Namen den Sie der Umgebung geben möchten: 
    `conda env create -f environment.yml -n Umgebungsname`

### Ausführung des Programms ###
Sie müssen Schritt 1) - 3) jedes mal nach Schließen der Anaconda Shell ausführen.
1) Öffnen Sie die Anaconda Shell.
2) Navigieren Sie mit der Shell in den src Ordner des heruntergeladenen Repositoryverzeichnisses.
3) Aktivieren Sie die erstellte Pythong Umgebung:
    `conda activate Umgebungsname`
4) Führen Sie das Skript aus:
    `python attachment.py -timeout 10 -ziel Pfad\\Zum\\Ziel\\Download\\Ordner\\ -treiber Pfad\\Zum\\geckodriver.exe -passwort ihrPasswort -username ihrNutzerName -url linkZuIhrerBaySimInstanz`

### Programm Argumente ###
Alle Argumente sind verpflichtend. Es gibt **keine** Möglichkeit eines oder mehrere der Argumente wegzulassen.

* -timeout : Gibt die maximale Zeit (in ganzen Sekunden) bis zum Timeout an. Wenn das benötigte Fenster in dieser plus der standardmäßig festgelegten Zeit nicht geladen hat, beendet sich das Programm. Der Standard Wert sind 5 Sekunden. Dieser Wert muss je nach Internetverbindungsgeschwindigkeit und Rechnergeschwindigkeit angepasst werden.

* -ziel : Dieses Argument gibt den Pfad zum Ordner an, in den Firefox alle Dateien automatisch herunterladen wird. *WICHTIG* ist, dass der Pfad mit \\\\ statt / angegeben wird und auch mit einem \\\\ beendet wird. Firefox wird sonst alle Dateien in den normalen Downloads Ordner herunterladen oder abstürzen wenn die \\\\ am Ende fehlen.

* -treiber : Dieses Argument gibt den Pfad zur ausführbaren Datei des Firefox Gecko Treibers an, der unter Benötigte Programme gelistet ist. *WICHTIG* ist, dass der Pfad mit \\\\ statt / angegeben wird und mit dem Dateinamen.Dateiendung beendet wird.

* -passwort : Hier müssen Sie Ihr Passwort für BaySim angeben.

* -username : Hier müssen Sie Ihren Benutzername für BaySim angeben.

* -url : Hier müssen Sie den Link zu Ihrer BaySim Instanz angeben
