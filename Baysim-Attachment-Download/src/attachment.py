#
# Code by Luca Hohmann (luca.hohmann@tum.de)
#

import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import os
import sys
import datetime

MAXIMUM_RETRYS = 10
NUMBER_DOWNLOADS = 0
START_TIME = 0
END_TIME = 0

def write_page_html_to_file(filename='html_source_code.html'):
    elem = driver.find_element_by_xpath("//*")
    source_code = elem.get_attribute("outerHTML")
    with open('./'+filename, 'w') as f:
        text = source_code.encode('utf-8')
        f.write(str(text))


def retry_until_loaded(such_string, search_type):
    element_reference = None
    search_type = search_type.lower()
    worked = False
    retry_counter = 0
    while not worked and retry_counter < MAXIMUM_RETRYS:
        try:
            if search_type == "xpath":
                element_reference = driver.find_element_by_xpath(such_string)
            elif search_type == "id":
                element_reference = driver.find_element_by_id(such_string)
            elif search_type == "tag":
                element_reference = driver.find_element_by_tag_name(such_string)
            elif search_type == "class":
                element_reference = driver.find_element_by_class_name(such_string)
            worked = True
        except:
            time.sleep(0.5)
            retry_counter += 1
    if not worked:
        print("Error: Die nächste Seite hat die maximale Ladezeit überschritten. Daher wird das Programm beendet")
        driver.quit()
        sys.exit(0)
    return element_reference


# loggt den user bei Sormas ein
def logInToServer(URL: str, username: str, password: str):
    # Login maske öffnen
    driver.get(URL)

    # normale Wartezeit
    time.sleep(3)

    # Versuche Elementreferenz zu bekommen. Falls Seite noch nicht fertig geladen, wird retried
    # Benutzername Feld
    anmeldung_benutzername = retry_until_loaded('//*[@id="i0116"]', "xpath")
    anmeldung_benutzername.send_keys(username)

    # Bestätigen Button für Benutzername
    anmeldung_username_button = retry_until_loaded('//*[@id="idSIButton9"]', "xpath")
    anmeldung_username_button.click()

    time.sleep(1)

    # Passwort Feld
    anmeldung_password = retry_until_loaded('//*[@id="i0118"]', "xpath")
    anmeldung_password.send_keys(password)

    # Bestätigen Button für Passwort
    anmeldung_pw_button = retry_until_loaded('//*[@id="idSIButton9"]', "xpath")
    anmeldung_pw_button.click()

    time.sleep(1)

    # Nicht angemeldet bleiben Button
    nicht_angemeldet_bleiben_button = retry_until_loaded('//*[@id="idBtn_Back"]', "xpath")
    nicht_angemeldet_bleiben_button.click()


# Fügt die Spalte Fallnummer bei der Anfrage hinzu
def select_additional_columns():
    add_column = retry_until_loaded("Mscrm.AdvancedFind.Groups.View.EditColumns-Medium", "id")
    add_column.click()

    time.sleep(2)

    overlay_iframe = retry_until_loaded("InlineDialog_Iframe", "id")
    driver.switch_to.frame(overlay_iframe)

    time.sleep(5)

    view_iframe = retry_until_loaded("viewEditor", "id")
    driver.switch_to.frame(view_iframe)

    add_column_view = retry_until_loaded("_TBAddColumnsoActivefalse", "id")
    add_column_view.click()
    driver.switch_to.parent_frame()
    driver.switch_to.parent_frame()

    time.sleep(2)

    column_frame = retry_until_loaded("InlineDialog1_Iframe", "id")
    driver.switch_to.frame(column_frame)

    fallakten_bezug_selec = retry_until_loaded('//option[@title="Bezug (Fallakte)"]', "xpath")
    fallakten_bezug_selec.click()

    time.sleep(2)

    fallnummer_selec = retry_until_loaded("ch_cluster_name", "id")
    fallnummer_selec.click()

    ok_button = retry_until_loaded("butBegin", "id")
    ok_button.click()

    driver.switch_to.parent_frame()
    overlay_iframe = retry_until_loaded("InlineDialog_Iframe", "id")
    driver.switch_to.frame(overlay_iframe)

    outer_ok_button = retry_until_loaded("butBegin", "id")
    outer_ok_button.click()


# navigiert den Nutzer zur Ergebnisse Seite
def navigate_to_attachment_page():
    export_button = retry_until_loaded('//img[@title ="Exporte"]', "xpath")
    export_button.click()

    time.sleep(2)

    window_before = driver.window_handles[0]
    create_view_button = retry_until_loaded('//button[@aria-label="Ansicht erstellen"]', "xpath")
    create_view_button.click()

    time.sleep(5)

    window_after = driver.window_handles[1]
    driver.switch_to.window(window_after)

    # Wir müssen in den iFrame wechseln, da dort die selection drinen liegt
    seq = driver.find_elements_by_tag_name('iframe')
    #print("No of frames present in the web page are: ", len(seq))
    # switch to the iframe
    driver.switch_to.frame(seq[0])

    selection_button = retry_until_loaded('//*[@id="slctPrimaryEntity"]', "xpath")
    selection_button.click()

    selection_button = retry_until_loaded('//option[@title="Notizen"]', "xpath")
    selection_button.click()

    # zurück zum window
    driver.switch_to.parent_frame()

    select_additional_columns()

    driver.switch_to.parent_frame()

    results_show = retry_until_loaded('//*[text()="Ergebnisse"]', "xpath")
    results_show.click()


# führt einen Doppelklick aus
def double_click(element):
    actionChain = ActionChains(driver)
    actionChain.double_click(element)
    actionChain.perform()


# Holt alle Zeilen auf der Ergebnisseite und gibt sie zurück
def get_rows_in_table(frame_move):
    if frame_move:
        seq = driver.find_elements_by_tag_name('iframe')
    #print("No of frames present in the web page are: ", len(seq))
    # switch to the iframe
        driver.switch_to.frame(seq[0])

        time.sleep(2)

        seq = retry_until_loaded('resultFrame', "id")
        driver.switch_to.frame(seq)
    return driver.find_elements_by_class_name('ms-crm-List-Row')


# Arbeitet jede Zeile ab und extrahiert Daten
def work_through_entries(move_into_frames, targetFolder):
    # Wir müssen in den iFrame wechseln, da dort die elemente drinnen liegen
    rows = get_rows_in_table(move_into_frames)
    #print("num rows", len(rows))
    number_rows = len(rows)
    last_visited_row = 0
    first = True
    i = 0
    while i < number_rows:
        row = rows[i]
        if first:
            first = False
            number_rows -= 1
        else:
            i += 1

        children = row.find_elements_by_xpath("./child::*")
        filename = children[3].find_elements_by_tag_name("span")

        if filename[0].text == "":
            print("Übersprungen, keine Datei für den Eintrag verfügbar")
            continue
        fallnummer = children[4].find_element_by_tag_name("span").text

        scroll_element = children[3].find_element_by_tag_name('nobr')
        scroll_element.click()

        time.sleep(0.5)

        double_click(children[2])
        return_value = download_file()

        last_visited_row += 1
        rows = get_rows_in_table(True)
        if return_value == -1:
            print("Keine Zugriffsrechte für folgende Datei:", filename[0].text)
            continue
            # Fallnummer an Datei hängen
        if fallnummer == "":
            continue
        os.rename(targetFolder + filename[0].text, targetFolder + fallnummer + "_" + filename[0].text)


# Lädt Datei herunter
def download_file():
    global NUMBER_DOWNLOADS
    window_before = driver.window_handles[1]
    window_after = driver.window_handles[2]
    driver.switch_to.window(window_after)
    time.sleep(2)
    try:
        file_download = driver.find_element_by_id('crmAttachment')
        file_download.click()
    except:
        driver.close()
        driver.switch_to.window(window_before)
        return -1
    # Extra Fenster zumachen
    NUMBER_DOWNLOADS += 1
    driver.close()
    driver.switch_to.window(window_before)
    return 1


# Sucht und klickt auf den Button der zur nächsten Seite führt
def go_to_next_page():
    next_page_button_image = retry_until_loaded('_nextPageImg', "id")
    a = next_page_button_image.get_attribute('disabled')
    if next_page_button_image.get_attribute('disabled') != "true":
        next_page_button_image.click()
        time.sleep(2)
        return 1
    else:
        return 0


def print_help():
    print("Das Programm muss die folgenden Argumente übergeben bekommen:\n"
          "-------------------------------------------------------------"
          "-timeout : Maximale Zeit (in ganzen Sekunden), die zusätzlich zu den festen Wartezeiten auf das Laden von Fenstern und Seiten gewartet werden soll. (Standard: 5)\n"
          "-ziel : Der Pfad zum Zielordner für die Dokumente. Mit \\\\ statt / angeben\n"
          "-treiber : Der Pfad zum Geckotreiber für Firefox\n"
          "-passwort : Das Passwort für Ihren BaySim Nutzer\n"
          "-username : Der Benutzername für Ihren BaySim Nutzer\n"
          "-url : Den Link zur BaySim Instanz Login Maske\n\n")


def read_input_variables():
    arguments = {
    }
    cmd_args = sys.argv
    for i in range(1, len(sys.argv), 2):
        if cmd_args[i] == "-hilfe" or cmd_args[i] == "-h":
            print_help()
            sys.exit(1)
        elif cmd_args[i] == "-ziel":
            arguments["targetFolder"] = sys.argv[i+1]
        elif cmd_args[i] == "-treiber":
            arguments["driverPath"] = sys.argv[i+1]
        elif cmd_args[i] == "-passwort":
            arguments["password"] = sys.argv[i+1]
        elif cmd_args[i] == "-url":
            arguments["url"] = sys.argv[i+1]
        elif cmd_args[i] == "-username":
            arguments["username"] = sys.argv[i+1]
        elif cmd_args[i] == "-timeout":
            arguments["timeout"] = sys.argv[i + 1]
        else:
            print("Unbekanntes Argument übergeben:", sys.argv[i], "mit Wert:", sys.argv[i+1])
    return arguments


START_TIME = datetime.datetime.now().replace(microsecond=0)
user_arguments = read_input_variables()
if len(user_arguments.items()) != 6:
    print("Fehler: Es sind nicht genug Argumente übergeben worden. Bitte versuchen Sie es erneut oder nutzen sie -h um die Hilfe zu öffnen")

# weil wir in 0.5s Intervallen prüfen ob die Seite geladen hat mal *
MAXIMUM_RETRYS = int(user_arguments["timeout"]) * 2

options = webdriver.FirefoxOptions()
options.set_preference("browser.download.folderList", 2)
options.set_preference("browser.download.dir", user_arguments["targetFolder"])
options.set_preference("browser.download.viewableInternally.enabledTypes", "")
options.set_preference("browser.download.manager.showWhenStarting", False)
options.set_preference("browser.helperApps.neverAsk.saveToDisk", "attachment/xml;application/octet-stream")#"application/pdf;text/plain;application/text;text/xml;application/xml")
options.set_preference("pdfjs.disabled", True)

driver = webdriver.Firefox(executable_path=user_arguments["driverPath"], options=options)
driver.set_window_size(3000, 1080)
logInToServer(user_arguments["url"], user_arguments["username"], user_arguments["password"])
time.sleep(10)
navigate_to_attachment_page()
counter = 0
while True:
    if counter == 0:
        work_through_entries(True, user_arguments["targetFolder"])
        counter += 1
    else:
        if go_to_next_page() == 0:
            # no next page
            break
        else:
            # go to next page
            work_through_entries(False, user_arguments["targetFolder"])
END_TIME = datetime.datetime.now().replace(microsecond=0)
print("Es wurden insgesamt", NUMBER_DOWNLOADS, "Dateien aus BaySim heruntergeladen. Dies hat (Stunden/Minuten/Sekunden)", (END_TIME-START_TIME), "in Anspruch genommen")
driver.quit()
