# Sormas Code #

Dieses Repository beinhaltet die Jupyter Notebooks welche exemplarisch darstellen wie Indexpersonen, Kontakte und Länder/Kreise in ein SORMAS konformes import Format umgewandelt werden können.

### Setup ###
#### Variante 1 (empfpohlen)
* Anaconda installieren
* Neues environment aus der environment.yml Datei heraus erzeugen

#### Variante 1 (nicht empfpohlen)
* Numpy und Pandas, sowie mögliche andere fehlende Bibliotheken installieren
